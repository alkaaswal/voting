package `in`.versionx.voting

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import java.util.ArrayList
import java.util.Locale

import `in`.versionx.forms.Util.Global
import `in`.versionx.forms.Util.PersistentDatabase
import android.os.Build
import android.widget.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task


/**
 * Created by developer on 13/9/16.
 */
class ListAdapter(context: Context, internal var pollList: ArrayList<Poll>) : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {
    private var context: Context
        private set
    private val filterData: ArrayList<Poll>? = null
    internal var loginSp: SharedPreferences //imgPathSharedPref;
    //ImageLoader orgimageLoader;
    internal var bitmap: Bitmap? = null
    internal var picBitmap: Bitmap? = null
    internal var type: String? = null
    var AllRadioGroup: ArrayList<RadioGroup>? = null


    init {
        this.context = context
        loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE)
        AllRadioGroup = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        val view = inflater.inflate(R.layout.poll_item, parent, false)

        // Return a new holder instance
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {

        val checkOutparams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        checkOutparams.setMargins(0, 20, 5, 20)
        viewHolder.quest!!.setText(pollList.get(position).getQuest())

        var radioGroup: RadioGroup? = null



        if (!pollList.get(position).isIsviewShowed) {

            pollList.get(position).isviewShowed = true
            radioGroup = RadioGroup(context)
            radioGroup!!.orientation = LinearLayout.VERTICAL
            radioGroup!!.tag = pollList.get(position)



            pollList.get(position).getAnsOptions().forEach { (key, value) ->

                val radioButton = RadioButton(context)
                radioButton.layoutParams = checkOutparams
                radioButton.tag = value
                radioButton.setTextSize(20f)
                radioButton.text = key
                radioGroup!!.addView(radioButton)
                //allRadioBtn.add(radioButton)
            }

            AllRadioGroup!!.add(radioGroup)


            viewHolder.btn_submit!!.setTag(radioGroup)

            viewHolder.ll_main?.addView(radioGroup)
        }

        // var selected: Int =   radioGroup.checkedRadioButtonId

        viewHolder.btn_submit!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {


                var radioGroup: RadioGroup = v!!.tag as RadioGroup
                val v = viewHolder.ll_main

                val rb = (v!!.findViewById(radioGroup.getCheckedRadioButtonId()) as RadioButton)

                val poll = radioGroup.getTag() as Poll
                val ans = rb.getTag() as String

                val hashMap = HashMap<String, Any>()
                hashMap.put("nm", loginSp.getString(Global.USER_NAME, "Mani"))
                hashMap.put("dt", System.currentTimeMillis())
                hashMap.put("ans", ans)

                PersistentDatabase.database(context, Global.SECONDARY_DB).getReference("voting/log")
                        .child(poll.getId())
                        .child(loginSp!!.getString(Global.USER_ID, "")).updateChildren(hashMap).addOnCompleteListener { task: Task<Void> ->
                            if (!task.isSuccessful) {
                                System.out.println("error is " + task.exception)
                            } else {
                                val hashMap = HashMap<String, Any>()
                                hashMap.put(poll.getId(), true)
                                radioGroup.removeAllViews()
                                radioGroup.removeAllViewsInLayout()
                                AllRadioGroup!!.remove(radioGroup)


                                //  viewHolder.main_poll!!.removeAllViews()
                                /*  pollList.remove(poll)
                                  notifyDataSetChanged()*/

                                PersistentDatabase.database(context, Global.SECONDARY_DB).getReference("voting/user")
                                        .child(loginSp!!.getString(Global.USER_ID, "")).child("q").updateChildren(hashMap)

                            }
                        }
            }
        })


    }

    override fun getItemCount(): Int {
        return pollList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ll_main: LinearLayout? = null;
        var main_poll: LinearLayout? = null
        var quest: TextView? = null
        var btn_submit: Button? = null


        init {

            ll_main = itemView.findViewById(R.id.ll_for_poll)
            quest = itemView.findViewById(R.id.tv_poll_question)
            btn_submit = itemView.findViewById(R.id.btn_submit)
            main_poll = itemView.findViewById(R.id.main_poll)


        }
    }


}


