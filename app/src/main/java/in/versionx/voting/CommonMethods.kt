package `in`.versionx.forms.Util

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.ConnectivityManager

import java.text.SimpleDateFormat
import java.util.Calendar
import android.net.wifi.WifiManager
import android.net.wifi.WifiInfo
import android.support.v4.content.ContextCompat.getSystemService


object CommonMethods {

    val firstDayOfMonth: Calendar
        get() {
            val today = Calendar.getInstance()
            today.set(Calendar.MILLISECOND, 0)
            today.set(Calendar.SECOND, 0)
            today.set(Calendar.MINUTE, 0)
            today.set(Calendar.HOUR_OF_DAY, 0)
            today.set(Calendar.DATE, 1)


            return today
        }

    val endTimeYesterDay: Calendar
        get() {
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -1)
            cal.set(Calendar.MILLISECOND, 999)
            cal.set(Calendar.SECOND, 59)
            cal.set(Calendar.MINUTE, 59)
            cal.set(Calendar.HOUR_OF_DAY, 23)

            return cal
        }


    fun isInternetWorking(context: Context?): Boolean {

        if (context != null) {

            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    return true
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    // connected to the mobile provider's data plan
                    return true
                }
            } else {
                // not connected to the internet
                return false
            }

            return false
        } else {
            return true
        }
    }

    fun getPackageName(context: Context): String? {
        var pInfo: PackageInfo? = null
        try {
            pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            // e.printStackTrace();
            return null
        }

        return pInfo!!.packageName
    }


    fun getAppVersionCode(context: Context): Int {
        var pInfo: PackageInfo? = null
        try {
            pInfo = context.packageManager.getPackageInfo(getPackageName(context), 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return pInfo!!.versionCode
    }


    fun getVerisonNumber(context: Context): String {
        var pInfo: PackageInfo? = null
        try {
            pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return pInfo!!.versionName
    }

    fun getCurrentTime(format: String): String {
        val c = Calendar.getInstance()
        //  System.out.println("Current time => " + c.getTime());
        val df = SimpleDateFormat(format)
        return df.format(c.time)
    }

    fun checkWifi(context: Context, loginSp: SharedPreferences): Boolean {
        val wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val numberOfLevels = 5
        val wifiInfo = wifiManager.connectionInfo
        if (wifiInfo.ssid.substring(1, wifiInfo.ssid.length - 1).equals(loginSp.getString(Global.WIFI, ""), true)) {
            val level = WifiManager.calculateSignalLevel(wifiInfo.rssi, numberOfLevels)
            if (level >= 3) {
                return true
            } else {
                return false
            }
        } else
            return false

    }


}
