package `in`.versionx.voting

import `in`.versionx.forms.Util.CommonMethods
import `in`.versionx.forms.Util.Global
import `in`.versionx.forms.Util.PersistentDatabase
import android.content.Context
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    var ll_for_poll: LinearLayout? = null
    var context: Context? = null
    var loginsp: SharedPreferences? = null
    var fielddbRef: DatabaseReference? = null
    var adapter: ListAdapter? = null
    var pollList: ArrayList<Poll>? = null
    var rvList: RecyclerView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initGUI()
    }

    private fun initGUI() {
        context = this
        rvList = findViewById(R.id.rv_polls)
        pollList = ArrayList<Poll>()
        loginsp = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE)
        adapter = ListAdapter(this, pollList!!)
        // Attach the toMeetadapter to the recyclerview to populate items
        rvList!!.setAdapter(adapter)
        //rvList!!.addItemDecoration(SimpleDividerItemDecoration(this))
        rvList!!.setLayoutManager(LinearLayoutManager(this))



        getFields()
    }


    private fun getFields() {

        PersistentDatabase.database(context!!, Global.SECONDARY_DB).getReference("voting/room").child("wifi").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {

                if (p0.exists()) {
                    loginsp!!.edit().putString(Global.WIFI, p0.getValue() as String).apply()
                }

            }

        })


        PersistentDatabase.database(this, Global.SECONDARY_DB).getReference("voting/user")
                .child(loginsp!!.getString(Global.USER_ID, "")).addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {


                        pollList!!.clear()
                        adapter!!.notifyDataSetChanged()
                    //    if (CommonMethods.checkWifi(context!!, loginsp!!)) {
                            if (p0.hasChild("p") && p0.child("p").getValue() as Boolean) {


                                getPoll(p0)

                      //      }
                        }


                    }


                })


    }


    fun getPoll(dataSnapshot1: DataSnapshot) {
        fielddbRef = PersistentDatabase.database(this, Global.SECONDARY_DB).getReference("voting/ques")

        fielddbRef!!.orderByChild("act")?.equalTo(true)?.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var poll: Poll? = null

                for (childs in dataSnapshot.children) {
                    if (!dataSnapshot1.hasChild("q") || (dataSnapshot1.hasChild("q") && !dataSnapshot1.child("q").hasChild(childs.key!!))) {

                        poll = Poll();

                        poll!!.setId(childs.key)
                        poll!!.setQuest(childs.child("nm").getValue().toString())


                        var optionList = HashMap<String, String>()

                        for (list in childs.child("opt").getChildren()) {
                            optionList.put(list.getValue().toString(), list.getKey()!!);
                        }
                        poll!!.setAnsOptions(optionList);



                        pollList!!.add(poll);
                    }

                }

                adapter!!.notifyDataSetChanged()

                // setFields(pollList)

            }


            override fun onCancelled(p0: DatabaseError) {

            }

        });
    }


}
