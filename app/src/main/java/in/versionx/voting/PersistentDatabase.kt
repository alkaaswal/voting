package `in`.versionx.forms.Util

import android.content.Context
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions


/**
 * Created by developer on 8/7/16.
 */
class PersistentDatabase {

    companion object {
        public var mDatabase: FirebaseDatabase? = null
        var secondaryDb: FirebaseDatabase? = null


        fun database(context: Context, whichDb: Int): FirebaseDatabase {
            if (whichDb == Global.PRIMARY_DB) {
                if (mDatabase == null) {
                    mDatabase = FirebaseDatabase.getInstance()
                    mDatabase!!.setPersistenceEnabled(true)
                }
                return mDatabase!!
            }
            else
            {
                if(secondaryDb==null)
                {
                    val options = FirebaseOptions.Builder()
                            .setApiKey("AIzaSyDoQ8FSN6I7I05mU2oFIYe407Qoekg2sWc")
                            .setApplicationId("1:154012533942:android:6a2c61c78f3f69f1")
                            .setDatabaseUrl("https://dev-entry-2.firebaseio.com")
                            .build()
                    val secondApp = FirebaseApp.initializeApp(context, options, "second app")
                    secondaryDb = FirebaseDatabase.getInstance(secondApp)
                }
                return secondaryDb!!
            }






        }
    }
}

