package `in`.versionx.voting

import `in`.versionx.forms.Util.CommonMethods
import `in`.versionx.forms.Util.Global
import `in`.versionx.forms.Util.PersistentDatabase
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.view.View
import android.view.View.OnClickListener
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId

import java.util.ArrayList
import java.util.Calendar
import java.util.HashMap




class VerificationActivity : Activity(), OnClickListener {

    private var btn_verify: Button?=null

    internal var sp: SharedPreferences?=null
    private var mAuth: FirebaseAuth? = null

    internal var alert: AlertDialog? = null
    internal var loginSp: SharedPreferences?=null

    internal var PLAY_SERVICES_RESOLUTION_REQUEST = 111
    internal var ed_email: EditText?=null
    internal var domain = "@versionx.in"
    var dialog :ProgressDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        initGUI() // initializing the UI elements
        setListners() // set event listners on UI elements
        checkPlayServices()

    }

    fun initGUI() {
        btn_verify = findViewById(R.id.btn_verification_verify) as Button
        ed_email = findViewById(R.id.ed_verificationEmail) as EditText
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        mAuth = FirebaseAuth.getInstance()
        loginSp = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE)
    }

    fun setListners() {
        btn_verify!!.setOnClickListener(this)


    }


    private fun checkPlayServices(): Boolean {
        val gApi = GoogleApiAvailability.getInstance()
        val resultCode = gApi.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                gApi.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show()
            } else {
                Toast.makeText(this, "Google Play Service issue!", Toast.LENGTH_LONG).show()
                finish()
            }
            return false
        }
        return true
    }

    override fun onClick(v: View) {

        when (v.id) {
            R.id.btn_verification_verify -> if (ed_email!!.text.toString().trim { it <= ' ' } == "") {
                Toast.makeText(applicationContext, "can't be blank!", Toast.LENGTH_SHORT).show()
                ed_email!!.setHintTextColor(Color.RED)
            }  else {
                //    ed_email.setHintTextColor(resources.getColor(R.color.app_text_color))
                //    ed_pass.setHintTextColor(resources.getColor(R.color.app_text_color))
                if (CommonMethods.isInternetWorking(this@VerificationActivity)) {
                    dialog = ProgressDialog(this@VerificationActivity)
                    dialog!!.show()
                    dialog!!.setMessage("Verifying...")
                    mAuth!!.signInWithEmailAndPassword("app@versionx.in", "Pass@123")
                            .addOnCompleteListener(this) { task ->

                                if (!task.isSuccessful) {
                                    Toast.makeText(this@VerificationActivity, "Invalid login!", Toast.LENGTH_SHORT).show()
                                    dialog!!.dismiss()
                                } else {

                                    PersistentDatabase.database(this, Global.SECONDARY_DB).getReference("voting/user")
                                            .orderByChild("mob").equalTo(ed_email!!.text.toString().trim()).addListenerForSingleValueEvent(object : ValueEventListener {
                                                override fun onCancelled(p0: DatabaseError) {


                                                }

                                                override fun onDataChange(p0: DataSnapshot) {
                                                    if (p0.exists()) {

                                                        for(child in p0.children) {

                                                            val intent = Intent(this@VerificationActivity, MainActivity::class.java)
                                                            `intent`.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                            `intent`.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                                            `intent`.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                                            loginSp!!.edit().putBoolean(Global.IS_LOGGED_IN, true).apply()
                                                            loginSp!!.edit().putString(Global.USER_ID, child.key).apply()
                                                            loginSp!!.edit().putString(Global.USER_NAME, child.child("nm").getValue()as String).apply()
                                                            finish()
                                                            startActivity(intent)
                                                      }



                                                    } else {
                                                        Toast.makeText(this@VerificationActivity, "Not registered!", Toast.LENGTH_SHORT).show()
                                                    }

                                                }

                                            })
                                }


                            }


                } else {
                    //  Toast.makeText(this, Global.NO_INTERNET_MSG, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }



    override fun onPause() {
        super.onPause()
        //      adminDb.removeEventListener(adminListener);
    }


}
