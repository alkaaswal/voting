package in.versionx.voting;

import java.util.HashMap;

public class Poll {

    String id, quest;
    boolean isviewShowed = false;

    public boolean isIsviewShowed() {
        return isviewShowed;
    }

    public void setIsviewShowed(boolean isviewShowed) {
        this.isviewShowed = isviewShowed;
    }

    HashMap<String, String> ansOptions;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setQuest(String quest) {
        this.quest = quest;
    }

    public String getQuest() {
        return quest;
    }

    public void setAnsOptions(HashMap<String, String> ansOptions) {
        this.ansOptions = ansOptions;
    }

    public HashMap<String, String> getAnsOptions() {
        return ansOptions;
    }
}
