package `in`.versionx.forms.Util

class Global {
    companion object {
        var BIZ_ID: String = "bizId"
        var GROUP_ID: String = "grpId"
        var LOGIN_SP: String = "loginSp"

        var TEXTBOX: String = "tb"
        var NUMBER: String = "num"
        var TEXTAREA: String = "ta"
        var DROPDOWN: String = "dd"
        var CHECKBOX: String = "cb"
        var RADIOBUTTON: String = "rb"
        var IMG: String = "img"
        var SEP: String = "sep"
        var QUESTION_SIZE: String = "qS"
        var ANSWER_SIZE: String = "aS"
        var FIELD_PADDING: String = "padding"
        var BACKGROUND_CLR: String = "bClr"
        var TEXT_CLR: String = "txtClr"
        var BUTTON_CLR = "btnClr"
        var DEVICE_ID: String = "devId"
        var Q_TEXT_CLR: String = "qClr"
        var A_TEXT_CLR: String = "aClr"
        var SEP_CLR: String = "sepClr"
        var HEAD_CLR: String = "hClr"
        var WIFI: String = "wifi"


        var NO_INTERNET_MSG = "No Internet Connection! Try again"
        var VERSION_NUMBER = "verisonNumber"
        val GROUP_NAME = "groupName"
        var BIZ_EXPIRY_DATE = "expiryDate"
        var BIZ_COMPANY_IMG = "ProfileImg"
        val UUID = "uuid"
        val APP_NAME = "voting"
        var IS_LOGGED_IN = "IsLoggedIn"
        var USER_ID = "uId"
        var USER_NAME = "userNm"

        var PRIMARY_DB = 0
        var SECONDARY_DB = 1
    }
}