package `in`.versionx.voting


import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar

import com.google.firebase.auth.FirebaseAuth

import `in`.versionx.forms.Util.Global
import `in`.versionx.voting.MainActivity
import `in`.versionx.voting.R
import `in`.versionx.voting.VerificationActivity
import android.content.Context


class SplashScreenActivity : Activity() {

    private var progressBar: ProgressBar? = null
    private var loginSp: SharedPreferences? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        initGUI()


        //       loadDataFromFirebase();

        Handler().postDelayed({
            if (FirebaseAuth.getInstance() != null && FirebaseAuth.getInstance().currentUser != null)
            // this means user has already verified the mobile number
            {

                val `in` = Intent(applicationContext, MainActivity::class.java)
                // Intent in = new Intent(getApplicationContext(), Activity_Print.class);
                `in`.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                `in`.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                finish()
                startActivity(`in`)
            } else {
                val `in` = Intent(applicationContext, VerificationActivity::class.java)
                `in`.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                `in`.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                finish()
                startActivity(`in`)
            }
        }, 5000)

    }

    fun initGUI() {
        progressBar = findViewById(R.id.pb_splashScreen)
        loginSp = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE)

        progressBar!!.indeterminateDrawable.setColorFilter(Color.parseColor("#009A9A"), android.graphics.PorterDuff.Mode.MULTIPLY)


    }


    override fun onResume() {
        super.onResume()


    }


}
